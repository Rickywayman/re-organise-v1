 //
//  SliderHP.h
//  JuceBasicWindow
//
//  Created by Ricky Wayman on 14/01/2016.
//
//

#ifndef __JuceBasicWindow__SliderHP__
#define __JuceBasicWindow__SliderHP__

//#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "HighPassFilter.h"

class SliderHP :    public Component,
                    private Slider::Listener
{
public:
    
    SliderHP (HighpassFilter& highpassFilter_);
    ~SliderHP();
    
    void resized() override;
    
    float getHighpassFeedbackGain();
    
    float getHighpassGain();
    
private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (SliderHP)
    
    void sliderValueChanged	(Slider * 	slider);
    
    HighpassFilter& highpassFilter;
    
    Slider gainHighSlider;
    Slider feedbackHighGainSlider;
    Label  gainHighLabel;
    Label feedbackHighGainLabel;
    
    float gain;
    float level;
    
};


#endif /* defined(__JuceBasicWindow__SliderHP__) */
