//
//  DelayLineFilter.h
//  JuceBasicWindow
//
//  Created by Ricky Wayman on 12/01/2016.
//
//

#ifndef __JuceBasicWindow__DelayLineFilter__
#define __JuceBasicWindow__DelayLineFilter__

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"


#define ASSUMEDSAMPLERATE 44100

class DelayLineFilter
{
public:
    //==============================================================================
    /**
     Constructor
     */
    DelayLineFilter();
    
    /**
     Destructor
     */
    virtual ~DelayLineFilter();
    //==============================================================================
    /**
     Set the delay time in samples of the delayline - can be fractional
     */
    void setDelayInSamples (float delayInSamples);
    
    /**
     Sets the feedback gain
     */
    void setFeedbackGain (float val);
    
    /**
     Function to perform the filter processing should call delayLineRead and delayLineWrite at some point
     in the process
     */
    virtual float filter (float input)=0;
    
protected:
    /**
     Reads from the delayline using linear interpolation - should be called once per filter call and prior
     to delayLineWrite or the delay will be one sample inacurate
     */
    float delayLineRead();
    
    /**
     Writes to the delayline - should be called once per filter call and after to delayLineRead or
     the delay will be one sample inacurate
     */
    void delayLineWrite(float input);
    float feedbackGain;
    
private:
    float delayLine[ASSUMEDSAMPLERATE];
    float readDelaySamples;
    int writeIndex;
    
    
    
};

#endif /* defined(__JuceBasicWindow__DelayLineFilter__) */
