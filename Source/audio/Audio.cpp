/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"

Audio::Audio()
{
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    
    //load the filePlayer into the audio source
    audioSourcePlayer.setSource (&filePlayer);
    
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}

void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here
}

void Audio::setLowpassGain(float val)
{
    //DBG ("Audio Lowpass Gain   " << val);
    lowpassGain = val;
}

void Audio::setHighpassGain(float val)
{
    //DBG ("Audio Highpass Gain   " << val);
    highpassGain = val;
}

void Audio::getComboBox(int val)
{
    //DBG("AudioComboBoxValue: " << val);
    comboBox = val;
    
}

/////////// When to use int Audio::getComboBox() orrrrr void Audio::getComboBox(int val)////////////

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    // get the audio from our file player - player puts samples in the output buffer
    audioSourcePlayer.audioDeviceIOCallback (inputChannelData, numInputChannels, outputChannelData, numOutputChannels, numSamples);
    
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    //float inSampL;
    //float inSampR;
    
    while(numSamples--)
    {
        float output = *inL;
        //float input = filePlayer.getNextAudioBlock;

        
        //float output = inSampL;
        
            if (comboBox == 1) // Lowpass
            {
                output = lowpassFilter.filter (output) * lowpassGain;
            }
            else if(comboBox == 2) // Highpass
            {
                output = (highpassFilter.filter (output) * highpassGain) + *inL;
                //output =  (highpassFilter.filter(output) * Highgain)     + *inL;
            }
            else if(comboBox == 3) // Butterworth
            {
                //inSampL = (butterworthFilter.filter(*outL) + butterworthFilter.forwardfilter(input)) * BWgain;
            }
            else if(comboBox == 4) // Bandpass
            {
                //inSampL = (lowpassFilter.filter(*outL) - (highpassFilter.filter(output) + *inL)) * gain;
            }
      
        //inSampR = *outL;
        
        *outL = output;//inSampL * 1.f;
        *outR = output;//inSampL * 1.f;
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    audioSourcePlayer.audioDeviceAboutToStart (device);
}

void Audio::audioDeviceStopped()
{
    audioSourcePlayer.audioDeviceStopped();
}